import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
import * as cors from 'cors';


admin.initializeApp();
// // Start writing Firebase Functions
// // https://firebase.google.com/docs/functions/typescript
//

const corsHandler = cors({ origin: true});

export const helloWorld = functions.https.onRequest((request, response) => {
   console.log('Depurandooooo');
   response.send("Hola Coders desde Cloud Functions!");
});

export const byeWorld = functions.https.onRequest((request, response) => {
    //functions.logger.info("Hello logs!", {structuredData: true});
    console.log('Byeeee');
    response.send("Bye Coders desde Cloud Functions!");
 });

 export const prueba = functions.https.onRequest((req, res) => {
   corsHandler(req, res, () => {
       let requestUid = req.body.uid;
       let authToken = validarCabecera(req);

       if (!authToken) {
           return res.status(401).send('{ "mensaje" : "prohibido" }');
       }

       return decodificarToken(authToken)
           .then(uid => {
               console.log('UID ' + uid);
               if (uid === requestUid) {
                   return res.status(200).send('{ "mensaje" : "exito" }');
               } else {
                   return res.status(403).send('{ "mensaje" : "prohibido" }');
               }
           });
   });

});

 function validarCabecera(req: any) {
   if (req.headers.authorization && req.headers.authorization.startsWith('Bearer ')) {
       return req.headers.authorization.split('Bearer ')[1]
   }
}

function decodificarToken(authToken: any) {
   return admin.auth().verifyIdToken(authToken)
       .then(tokenDecoficado => {
           return tokenDecoficado.uid;
       });
}

 export const removerFoto = functions.firestore
   .document('platos/{id}')
   .onDelete((snap, context) =>{

      const id  = context.params.id;
      console.log('IDSTORAGE: ' + id);

      const storage = admin.storage();
      const bucket = storage.bucket();
      const file = bucket.file(`platos/${id}`);

      return file.delete();
   });

