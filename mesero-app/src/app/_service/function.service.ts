import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AngularFireAuth } from '@angular/fire/auth';

@Injectable({
  providedIn: 'root'
})
export class FunctionService {

  constructor(private http: HttpClient, private afa: AngularFireAuth) { }

  probar(){
    const url = "https://us-central1-mesero-app-95fdc.cloudfunctions.net/prueba";
    //return this.http.get(url);

    return this.afa.auth.currentUser.getIdToken().then(authToken =>{
      //console.log(authToken)
      const header = new HttpHeaders({'Authorization' : 'Bearer ' + authToken});
      const body = {uid: this.afa.auth.currentUser.uid};

      //console.log(body);

      return this.http.post(url, body, { headers: header} ).toPromise();

    });
  }

}
