import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import * as moment from 'moment';

@Injectable({
  providedIn: 'root'
})
export class ConsultaService {

  constructor(private afs: AngularFirestore) { }

  listar(fecha: Date){
    let inicio = moment(fecha).toISOString(); //ISO-DATE
    let fin = moment(inicio).add(1, 'days').toISOString();

    return this.afs.collection('consumos', ref => ref.where('fechaPedido', '>=', new Date(inicio))
      .where('fechaPedido','<', new Date(fin))).snapshotChanges();
  }
}
