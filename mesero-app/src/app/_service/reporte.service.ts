import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import * as moment from 'moment'; 
import { Consumo } from '../_model/consumo';
import { switchMap, first } from 'rxjs/operators';
import { forkJoin } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ReporteService {

  constructor(private afs: AngularFirestore) { }

  buscarPorFecha(fecha: Date){
    let inicio = moment(fecha).toISOString(); //ISO-DATE
    let fin = moment(inicio).add(1, 'days').toISOString();

    return this.afs.collection('consumos', ref => ref.where('fechaPedido', '>=', new Date(inicio))
      .where('fechaPedido','<', new Date(fin))).valueChanges();

  }

  //consumos por cliente
  buscarPorCliente() {
    return this.afs.collection<Consumo>('consumos', ref => ref.where('cliente.nombreCompleto', '==', 'Nestor Soto')).valueChanges();
  }

  //obtener datos del cliente por medio del consumo
  buscarClientePorConsumo() {
    return this.afs.collection<Consumo>('consumos', ref => ref.where('cliente.nombreCompleto', '==', 'Nestor Soto')).valueChanges()
      .pipe(switchMap((data: any) => {
        console.log(data);
        let idCliente = data[0].cliente.id
        return this.afs.collection('clientes').doc(idCliente).valueChanges();
      }));
  }

  //forkJoin
  buscarConsumosClienteIden() {
    let arreglo = [];
    let obs1 = this.afs.collection('clientes', ref => ref.where('dni', '==', '0000000')).valueChanges().pipe(first());
    let obs2 = this.afs.collection('clientes', ref => ref.where('dni', '>', '0000000')).valueChanges().pipe(first());
    
    arreglo.push(obs1);
    arreglo.push(obs2);

    //return forkJoin(obs1, obs2);
    return forkJoin(arreglo);
  }
}
