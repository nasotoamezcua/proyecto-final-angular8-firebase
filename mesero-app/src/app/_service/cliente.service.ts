import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Cliente } from './../_model/cliente';

@Injectable({
  providedIn: 'root'
})
export class ClienteService {

  constructor(private afs : AngularFirestore) { }

  listar(){
    return this.afs.collection<Cliente>('clientes').valueChanges();
  }

  registrar(cliente: Cliente){
    //Se generan id diferentes, tanto para el id documento como para el id del campo 
    // return this.afs.collection('clientes').add(cliente);
    
     //Se generan id iguales, tanto para el id documento como para el id del campo
    return this.afs.collection('clientes').doc(cliente.id).set(Object.assign({},cliente));
  }

  leer(documentId){
    return this.afs.collection<Cliente>('clientes').doc(documentId).valueChanges();
  }

  actualizar(cliente: Cliente){
    return this.afs.collection('clientes').doc(cliente.id).set(Object.assign({},cliente));
  }

  eliminar(cliente : Cliente){
    return this.afs.collection('clientes').doc(cliente.id).delete();
  }


}
