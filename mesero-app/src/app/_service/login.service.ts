import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';
import { Usuario } from '../_model/usuario';
import { Router } from '@angular/router';
import { auth } from 'firebase/app';
import { Observable, EMPTY } from 'rxjs';
import { switchMap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  user: Observable<Usuario>;

  constructor(
    private afa: AngularFireAuth, 
    private afs: AngularFirestore,
    private router: Router) {
      this.user = this.afa.authState.pipe(
        switchMap(user => {
          if(user){
            return this.afs.doc<Usuario>(`usuarios/${user.uid}`).valueChanges();
          }else{
            return EMPTY;
          }
        })
        )
     }

  login(usuario: string, clave: string){
    console.log('Entra a login Service');
    return this.afa.auth.signInWithEmailAndPassword(usuario,clave).then(res =>{
      console.log(res);
      this.actulizarUsuarioData(res.user);
    })
  }

  loginFacebook(){
    const provider = new auth.FacebookAuthProvider();
    return this.oAuthLogin(provider);
  }

  loginGoogle(){
    const provider = new auth.GoogleAuthProvider();
    return this.oAuthLogin(provider);
  }

  restablecerClave(email: string){
    return this.afa.auth.sendPasswordResetEmail(email);
  }

  registrarUsuario(usuario: string, clave:string){
    return this.afa.auth.createUserWithEmailAndPassword(usuario, clave);
  }

  cerrarSesion(){
    return this.afa.auth.signOut().then(()=>{
      //window.location.reload();
      this.router.navigate(['login']);
    });
  }

  estaLogeado(){
    return this.afa.auth.currentUser != null;
  }

  private oAuthLogin(provider: any){
    return this.afa.auth.signInWithPopup(provider).then( credencial =>{
      console.log('oAuthLogin');
      console.log(credencial);
      this.actulizarUsuarioData(credencial.user);
    });
  }

  private actulizarUsuarioData(usuario: any) {

    console.log('Entro a actulizarUsuarioData service');

    const userRef: AngularFirestoreDocument<Usuario> = this.afs.doc(`usuarios/${usuario.uid}`);

    /*
     ELIMINA EL ERROR DE LIBERAR LOS RECURSOS DE FIREBASE: "Missing or insufficient permissions"
     PERO YA NO SE PODRA REGISTRAR USUARIOS NUEVOS EN LA COLECCION USUARIOS DE LA BASE DE DAROS DE FIRESTORE 
     */
    let obj = userRef.valueChanges().subscribe(data => {
   
    //userRef.valueChanges().subscribe(data => {
      if (data) {
        console.log('Hay data');
        const datos: Usuario = {
          uid: usuario.uid,
          email: usuario.email,
          roles: data.roles
        }
        return userRef.set(datos);
      } else {
        console.log('No hay data');
        const datos: Usuario = {
          uid: usuario.uid,
          email: usuario.email,
          roles: ['USER']
        }
        return userRef.set(datos);
      }
    });

    //});
    
    obj.unsubscribe();
    
  }
}
