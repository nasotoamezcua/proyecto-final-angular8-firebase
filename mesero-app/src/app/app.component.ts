import { Component, OnInit, OnDestroy } from '@angular/core';
import { LoginService } from './_service/login.service';
import { Menu } from './_model/menu';
import { MenuService } from './_service/menu.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy{
  title = 'mesero-app';

  private ngUnsubscribe: Subject<void> = new Subject();

  menus: Menu[] = [];

  constructor(public loginService: LoginService, private menuService: MenuService) { }
  

  ngOnInit() {
    this.menuService.menuCambio.pipe(takeUntil(this.ngUnsubscribe)).subscribe(data =>{
      this.menus = data;
    });
   
  }

  ngOnDestroy() {
   this.ngUnsubscribe.next();
   this.ngUnsubscribe.complete();
  }

}
