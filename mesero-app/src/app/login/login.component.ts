import { Component, OnInit, OnDestroy } from '@angular/core';
import { LoginService } from '../_service/login.service';
import { Router } from '@angular/router';
import { auth } from 'firebase/app';
import { MenuService } from '../_service/menu.service';
import { Menu } from '../_model/menu';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit, OnDestroy {

  private ngUnsubscribe: Subject<void> = new Subject();

  usuario: string;
  clave: string;

  estadoLogin: boolean = true;
  estadoRecuperar: boolean;
  estadoCrear: boolean;
 

  constructor(
    private loginService: LoginService, 
    private router: Router,
    private menuService: MenuService) { }

  ngOnInit() {
   
  }

  login(){
    console.log('Entra a login componente');
    this.loginService.login(this.usuario, this.clave).then(() =>{
      this.listarMenus();
    }).catch( err =>{
      console.log('Error de login componente');
      console.log(err);
    });
  }

  loginFacebook(){
    this.loginService.loginFacebook().then( () =>{
      this.listarMenus();

    }).catch( err =>{
      if (err.code === 'auth/account-exists-with-different-credential') {
        let facebookCred = err.credential;
        let googleProvider = new auth.GoogleAuthProvider();
        googleProvider.setCustomParameters({ 'login_hint': err.email });

        return auth().signInWithPopup(googleProvider).then(result => {
          return result.user.linkWithCredential(facebookCred);
        });
      }

    });
  }

  loginGoogle(){
    this.loginService.loginGoogle().then(() =>{
      this.listarMenus();
    });
  }

  restablecerClave(){
    this.loginService.restablecerClave(this.usuario).then( data =>{
      console.log(data);
    });
  }

  crearUsuario(){
    this.loginService.registrarUsuario(this.usuario, this.clave);
    this.irLogin();
  }

  irLogin(){
    this.estadoLogin = true;
    this.estadoCrear = false;
    this.estadoRecuperar = false;
  }

  irCrear(){
    this.estadoCrear = true;
    this.estadoLogin = false;
    this.estadoRecuperar = false;
  }

  irRecuperar(){
    this.estadoRecuperar = true;
    this.estadoCrear = false;
    this.estadoLogin = false;
  }

  listarMenus() {
    console.log('Entro a listar menus');
    this.menuService.listar().pipe(takeUntil(this.ngUnsubscribe)).subscribe(menus => {
      console.log('Ejecuta listar  de menu service');

      this.loginService.user.pipe(takeUntil(this.ngUnsubscribe)).subscribe(userData => {
        console.log('verifica user  de menu service');
        if (userData) {
          //console.log(userData);
          let user_roles: string[] = userData.roles
          let final_menus: Menu[] = [];

          for (let menu of menus) {
            n2: for (let rol of menu.roles) {
              for (let urol of user_roles) {
                if (rol === urol) {
                  let m = new Menu();
                  m.nombre = menu.nombre;
                  m.icono = menu.icono;
                  m.url = menu.url;
                  final_menus.push(m);
                  break n2;
                }
              }
            }

            this.menuService.menuCambio.next(final_menus);
            this.router.navigate(['plato']);
          }
        }
      });
    });
  }

  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
   }
}
