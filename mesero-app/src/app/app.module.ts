import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './material/material.module';
import { PlatoComponent } from './pages/plato/plato.component';

import { AngularFireModule } from '@angular/fire';
import { FirestoreSettingsToken, AngularFirestore, AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { AngularFireAuthModule } from '@angular/fire/auth';

import { environment } from './../environments/environment';
import { PlatoEdicionComponent } from './pages/plato/plato-edicion/plato-edicion.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { PlatoDialogoComponent } from './pages/plato/plato-dialogo/plato-dialogo.component';
import { ClienteComponent } from './pages/cliente/cliente.component';
import { ConsultaComponent } from './pages/consulta/consulta.component';
import { ConsumoComponent } from './pages/consumo/consumo.component';
import { PerfilComponent } from './pages/perfil/perfil.component';
import { ReporteComponent } from './pages/reporte/reporte.component';
import { CompaComponent } from './pages/demo/compa/compa.component';
import { CompbComponent } from './pages/demo/compb/compb.component';
import { DialogoComponent } from './pages/consulta/dialogo/dialogo.component';
import { LoginComponent } from './login/login.component';
import { Not403Component } from './pages/not403/not403.component';
import { HttpClientModule } from '@angular/common/http';
import { LocationStrategy, HashLocationStrategy } from '@angular/common';
import { ClienteEdicionComponent } from './pages/cliente/cliente-edicion/cliente-edicion.component';

@NgModule({
  declarations: [
    AppComponent,
    PlatoComponent,
    PlatoEdicionComponent,
    PlatoDialogoComponent,
    ClienteComponent,
    ConsultaComponent,
    ConsumoComponent,
    PerfilComponent,
    ReporteComponent,
    CompaComponent,
    CompbComponent,
    DialogoComponent,
    LoginComponent,
    Not403Component,
    ClienteEdicionComponent
  ],
  imports: [  
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    AngularFirestoreModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireStorageModule,
    AngularFireAuthModule
    
  ],

  entryComponents:[
    PlatoDialogoComponent,
    DialogoComponent
  ],

  providers: [
    AngularFirestore,
    {provide: FirestoreSettingsToken, useValue: {}},
    {provide: LocationStrategy, useClass: HashLocationStrategy}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
