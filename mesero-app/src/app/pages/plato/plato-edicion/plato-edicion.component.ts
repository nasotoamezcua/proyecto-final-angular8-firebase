import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Plato } from './../../../_model/plato';
import { PlatoService } from 'src/app/_service/plato.service';
import { ActivatedRoute, Params, Router } from '@angular/router';

import { MatSnackBar } from '@angular/material';
import { AngularFireStorage } from '@angular/fire/storage';
import { AngularFirestore } from '@angular/fire/firestore';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';


@Component({
  selector: 'app-plato-edicion',
  templateUrl: './plato-edicion.component.html',
  styleUrls: ['./plato-edicion.component.css']
})
export class PlatoEdicionComponent implements OnInit, OnDestroy {

  private ngUnsubscribe: Subject<void> = new Subject();

  //files :any[];
  file : any;
  labelFIle : string;
  urlImagen : string;

  id: string;
  form : FormGroup;
  edicion: boolean;

  constructor(
      private platoService: PlatoService, 
      private route: ActivatedRoute, 
      private router: Router,
      private snackBar: MatSnackBar,
      private afStorage: AngularFireStorage,
      private afs : AngularFirestore) { }

  ngOnInit() {
    this.form = new FormGroup({
      'id' : new FormControl(''),
      'nombre' : new FormControl('',[Validators.required, Validators.minLength(2)]),
      'precio' : new FormControl(0, Validators.required),
    });

    this.route.params.subscribe((params:Params) =>{
      this.id = params['id'];
      this.edicion = this.id != null;
      this.initForm();
    });
  }

  get f (){ return this.form.controls; }

  initForm(){
    if(this.edicion){
      this.platoService.leer(this.id).pipe(takeUntil(this.ngUnsubscribe)).subscribe((data:Plato) =>{
        this.form = new FormGroup({
          'id' : new FormControl(data.id),
          'nombre' : new FormControl(data.nombre),
          'precio' : new FormControl(data.precio),
        });

        if(data.id != null){
          this.afStorage.ref(`platos/${data.id}`).getDownloadURL().subscribe(data =>{
            this.urlImagen = data;
          });
        }


      });
    }
  }

  operar(){
    if (!this.form.invalid) {
      let plato = new Plato();

      if(this.edicion){
        plato.id = this.form.value['id'];
      }else{
        plato.id = this.afs.createId();
      }
  
      plato.nombre = this.form.value['nombre'];
      plato.precio = this.form.value['precio'];
  
      if(this.file != null){
        let ref = this.afStorage.ref(`platos/${plato.id}`);
        ref.put(this.file);
      }
  
      let mensaje;
  
      if(this.edicion){
        this.platoService.modificar(plato);
        mensaje = 'SE MODIFICO';
      }else{
        this.platoService.registrar(plato);
        mensaje = 'SE REGISTRO';
      }
  
      this.snackBar.open(mensaje, 'AVISO',{
        duration: 2000
      });
  
      this.router.navigate(['plato']);
    }
  }

  seleccionar(e: any){
    //this.files = e.target.files;
    this.file = e.target.files[0];
    this.labelFIle = e.target.files[0].name;

  }

  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
   }

}
