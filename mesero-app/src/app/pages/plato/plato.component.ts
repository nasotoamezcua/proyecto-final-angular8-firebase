import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { PlatoService } from 'src/app/_service/plato.service';
import { Plato } from './../../_model/plato';
import { MatTableDataSource, MatPaginator, MatSort, MatSnackBar, MatDialog } from '@angular/material';
import { PlatoDialogoComponent } from './plato-dialogo/plato-dialogo.component';
import { ActivatedRoute } from '@angular/router';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { FunctionService } from 'src/app/_service/function.service';


@Component({
  selector: 'app-plato',
  templateUrl: './plato.component.html',
  styleUrls: ['./plato.component.css']
})
export class PlatoComponent implements OnInit, OnDestroy {

  private ngUnsubscribe: Subject<void> = new Subject();

  dataSource: MatTableDataSource<Plato>;
  displayedColumns: string[] = ['nombre', 'precio', 'acciones'];

  @ViewChild(MatPaginator, { static : false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort : MatSort;

  constructor(
    private platoService : PlatoService, 
    private snackBar: MatSnackBar,
    private dialog : MatDialog,
    public activateRoute: ActivatedRoute,
    private funcioService: FunctionService) { }

  ngOnInit() {

    this.funcioService.probar().then(data =>{
      console.log(data);
    });

    this.platoService.listar().pipe(takeUntil(this.ngUnsubscribe)).subscribe(data =>{
      this.dataSource = new MatTableDataSource(data);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      console.log(data);
    });
  }

  eliminar(plato: Plato){
    this.platoService.eliminar(plato).then(() =>{
      this.snackBar.open('SE ELIMINO', 'AVISO',{
        duration: 2000,
      });
    });

  }

  abrirDialogo(plato: Plato){
    this.dialog.open(PlatoDialogoComponent,{
      width: '250px',
      data: plato
    })
  }

  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
   }

}
