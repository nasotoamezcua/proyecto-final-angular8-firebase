import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

import { Plato } from './../../../_model/plato';

@Component({
  selector: 'app-plato-dialogo',
  templateUrl: './plato-dialogo.component.html',
  styleUrls: ['./plato-dialogo.component.css']
})
export class PlatoDialogoComponent implements OnInit {

  plato:Plato;

  constructor(public dialogRef: MatDialogRef<PlatoDialogoComponent>, 
    @Inject(MAT_DIALOG_DATA) public data: Plato) { }

  ngOnInit() {
    this.plato = new Plato();
    this.plato.id = this.data.id;
    this.plato.nombre = this.data.nombre;
    this.plato.precio = this.data.precio;
  }

  cancelar(){
    this.dialogRef.close();
  }

}
