import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Consumo } from 'src/app/_model/consumo';
import { AngularFireStorage } from '@angular/fire/storage';

@Component({
  selector: 'app-dialogo',
  templateUrl: './dialogo.component.html',
  styleUrls: ['./dialogo.component.css']
})
export class DialogoComponent implements OnInit {

  urlImagen: string;
  clienteId: string;

  consumo: Consumo;

  constructor(
    private dialogRef: MatDialogRef<DialogoComponent>,
    private afStorage :AngularFireStorage, 
    @Inject(MAT_DIALOG_DATA) public data: Consumo) { }

  ngOnInit() {
    this.consumo = this.data;
    this.clienteId = this.consumo.cliente.id;
    this.afStorage.ref(`clientes/${this.clienteId}`).getDownloadURL().subscribe(data =>{
     
      if(data != null){
        console.log(data);
        this.urlImagen = data;
      }
  });
 }

cerrar(){
  this.dialogRef.close();
 }
}

