import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { ConsultaService } from 'src/app/_service/consulta.service';
import { MatTableDataSource, MatPaginator, MatSort, MatDialog } from '@angular/material';
import { Consumo } from 'src/app/_model/consumo';
import { DialogoComponent } from './dialogo/dialogo.component';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-consulta',
  templateUrl: './consulta.component.html',
  styleUrls: ['./consulta.component.css']
})
export class ConsultaComponent implements OnInit, OnDestroy {

  private ngUnsubscribe: Subject<void> = new Subject();

  dataSource: MatTableDataSource<Consumo>;
  displayedColumns = ['cliente', 'fechaPedido', 'total', 'acciones'];
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  fechaSeleccionada: Date = new Date();


  constructor(private consultaService: ConsultaService, private dialog: MatDialog) { }

  ngOnInit() {
    
  }

  buscar(){
    this.consultaService.listar(this.fechaSeleccionada).pipe(takeUntil(this.ngUnsubscribe)).subscribe(snapshot =>{
      let consumos = [];
      snapshot.forEach( (data: any) =>{
        let consumo = new Consumo();
        consumo.id = data.payload.doc.id;
        //console.log(data.payload.doc.data());
        consumo.cliente = data.payload.doc.data().cliente;
        consumo.total = data.payload.doc.data().total;
        consumo.fechaPedido = data.payload.doc.data().fechaPedido.toDate();
        consumo.detalle = data.payload.doc.data().detalle;
        consumos.push(consumo);
      });

      this.dataSource = new MatTableDataSource(consumos);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;

    });
  }

  verDetalle(consumo: Consumo){
    this.dialog.open(DialogoComponent,{
      width: '400px',
      data: consumo
    });
  }

  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
   }

}
