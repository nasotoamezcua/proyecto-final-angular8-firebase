import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';

import { Observable, Subject } from 'rxjs';
import { ClienteService } from 'src/app/_service/cliente.service';
import { PlatoService } from './../../_service/plato.service';
import { Cliente } from 'src/app/_model/cliente';
import { Plato } from 'src/app/_model/plato';
import { map, takeUntil } from 'rxjs/operators';
import { Detalle } from 'src/app/_model/detalle';
import { MatTableDataSource, MatPaginator, MatSort, MatSnackBar } from '@angular/material';
import { Consumo } from 'src/app/_model/consumo';
import { ConsumoService } from 'src/app/_service/consumo.service';
import { AngularFirestore } from '@angular/fire/firestore';

@Component({
  selector: 'app-consumo',
  templateUrl: './consumo.component.html',
  styleUrls: ['./consumo.component.css']
})
export class ConsumoComponent implements OnInit, OnDestroy {

  private ngUnsubscribe: Subject<void> = new Subject();
  
  form : FormGroup;
  ctrlCliente: FormControl = new FormControl();
  ctrlPlato: FormControl = new FormControl();

  clientesFiltrados: Observable<any[]>;
  platosFiltrados: Observable<any[]>;

  clientes: Cliente[] = [];
  platos: Plato[] = [];
  cliente: Cliente;
  plato: Plato;
  
  cantidad: number;
  total: number = 0;
  detalle: Detalle[] = [];


  dataSource: MatTableDataSource<Detalle>;
  displayedColumns = ['nombre','precio','cantidad','subtotal', 'acciones'];

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(
      private consumoService: ConsumoService,
      private clienteService: ClienteService, 
      private afStore: AngularFirestore,
      private platoService: PlatoService,
      private snackBar: MatSnackBar) { }

  ngOnInit() {
    this.form = new FormGroup({
      'cliente' : this.ctrlCliente,
      'plato' : this.ctrlPlato,
      'fecha' : new FormControl(new Date()),
      'cantidad' : new FormControl(0)
    });

    this.listarClientes();
    this.listarPlatos();

    this.clientesFiltrados = this.ctrlCliente.valueChanges.pipe(map(val => this.filtrarClientes(val)));
    this.platosFiltrados = this.ctrlPlato.valueChanges.pipe (map(val => this.filtrarPlatos(val)));
  }


listarClientes(){
  this.clienteService.listar().pipe(takeUntil(this.ngUnsubscribe)).subscribe(data =>{
    this.clientes = data;
  });
}

listarPlatos(){
  this.platoService.listar().pipe(takeUntil(this.ngUnsubscribe)).subscribe(data =>{
    this.platos = data;
  });
 }

 filtrarClientes(val :any){

  if(val != null && val.dni != null){
    return this.clientes.filter(cliente =>
      cliente.nombreCompleto.toLowerCase().includes(val.nombreCompleto.toLowerCase()) || cliente.dni.includes(val.dni));
  }else{
    return this.clientes.filter( cliente => 
      cliente.nombreCompleto.toLowerCase().includes(val.toLowerCase()) || cliente.dni.includes(val));
  }
 }

 filtrarPlatos(val: any){
  if(val != null && val.nombre != null){
    return this.platos.filter(plato =>
      plato.nombre.toLowerCase().includes(val.nombre.toLowerCase()));
  }else{
    return this.platos.filter( plato => 
      plato.nombre.toLowerCase().includes(val.toLowerCase()));
  }
 }

 mostrarSeleccionCliente(val: Cliente){
   return val ? `${val.nombreCompleto}` : val;
  }

 mostrarSeleccionPlato(val: Plato){
  return val ? `${val.nombre}` : val; 
 }

 seleccionarCliente(e: any){
  this.cliente = e.option.value;
  this.ctrlCliente.disable();
 }

 seleccionarPlato(e: any){
  
  this.plato = e.option.value;
 }

 habilitarCliente(){
  this.ctrlCliente.enable();
 }

 agregar(){
  let det = new Detalle();
  det.plato = this.plato;
  det.cantidad = this.cantidad;

  this.detalle.push(det);
  this.total += det.plato.precio * det.cantidad;

  this.dataSource = new MatTableDataSource(this.detalle);
  this.dataSource.paginator = this.paginator;
  this.dataSource.sort = this.sort;
}

remover(det: Detalle){
  this.total -= det.plato.precio * det.cantidad;

  let indices = [];
   for(let i =0; i < this.detalle.length; i++){
     this.detalle[i]._index = i;
     indices.push(i);
     console.log(indices);
   }

   let index = indices.indexOf(det._index);

   this.detalle.splice(index,1);

   this.dataSource = new MatTableDataSource(this.detalle);
   this.dataSource.paginator = this.paginator;
   this.dataSource.sort = this.sort;
}


aceptar(){
    let consumo = new Consumo();
    consumo.detalle = this.detalle;
    consumo.fechaPedido = new Date();
    consumo.total = this.total;

    if(this.cliente == undefined){
      this.cliente = new Cliente();
      let nombreCompleto = this.form.value['cliente'];
      this.cliente.nombreCompleto = nombreCompleto;
      this.cliente.dni = '0000000';
      this.cliente.id = this.afStore.createId();

      this.clienteService.registrar(Object.assign({}, this.cliente)).then(data =>{
        consumo.cliente = this.cliente; 

        this.consumoService.registar(consumo).then(()=>{
          this.emitirMensaje();
        });
      });

    }else{
      consumo.cliente = this.cliente;
      this.consumoService.registar(consumo).then(()=>{
        this.emitirMensaje();
      })
    }

    consumo.cliente = this.cliente;

}

aceptarTransaccion(){
  let consumo = new Consumo();
  consumo.detalle = this.detalle;
  consumo.fechaPedido = new Date();
  consumo.total = this.total;

  if(this.cliente == undefined){
    this.cliente = new Cliente();
    let nombreCompleto = this.form.value['cliente'];
    this.cliente.nombreCompleto = nombreCompleto;
    this.cliente.dni = '0000000';
  }

  consumo.cliente = this.cliente;

  this.consumoService.registrarTransaccion(consumo, this.cliente).then(() =>{
    this.emitirMensaje();
  });
}

emitirMensaje(){
  this.snackBar.open("Se registro exitosamente", "Aviso",{
    duration: 5000,
  });

  setTimeout(() =>{
    this.limpiar();
  }, 2000);
}

limpiar(){
  this.detalle = [];
  this.dataSource = new MatTableDataSource(this.detalle);
  this.cliente = new Cliente();
  this.total = 0;

  this.form = new FormGroup({
    'cliente' : this.ctrlCliente,
    'plato' : this.ctrlPlato,
    'fecha' : new FormControl(new Date()),
    'cantidad' : new FormControl(0)
  });

 }

ngOnDestroy() {
  this.ngUnsubscribe.next();
  this.ngUnsubscribe.complete();
 }

}

