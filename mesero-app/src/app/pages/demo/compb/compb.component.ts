import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';


@Component({
  selector: 'app-compb',
  templateUrl: './compb.component.html',
  styleUrls: ['./compb.component.css']
})
export class CompbComponent implements OnInit {

  @Input() mensaje: string

  @Output() emisor = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  enviar(){
    this.emisor.emit('Enviado desde Comp B');
  }

}
