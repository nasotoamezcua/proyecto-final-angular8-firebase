import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-compa',
  templateUrl: './compa.component.html',
  styleUrls: ['./compa.component.css']
})
export class CompaComponent implements OnInit {

  x = 'mensaje desde comp A';
  recpcion : string;

  constructor() { }

  ngOnInit() {
  }

  recibirEvento(e: any){
    this.recpcion = e;
  }

}
