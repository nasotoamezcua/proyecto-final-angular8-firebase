import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ClienteService } from 'src/app/_service/cliente.service';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { takeUntil } from 'rxjs/operators';
import { Cliente } from 'src/app/_model/cliente';
import { AngularFireStorage } from '@angular/fire/storage';
import { AngularFirestore } from '@angular/fire/firestore';
import { MatSnackBar } from '@angular/material';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-cliente-edicion',
  templateUrl: './cliente-edicion.component.html',
  styleUrls: ['./cliente-edicion.component.css']
})
export class ClienteEdicionComponent implements OnInit, OnDestroy {

  private ngUnsubscribe: Subject<void> = new Subject();
  

  file : any;
  labelFIle : string;
  urlImagen : string;

  id: string;
  form : FormGroup;
  edicion: boolean;

  constructor(
    private clienteService: ClienteService,
    private afStote: AngularFirestore,
    private afStorage: AngularFireStorage,
    private snackBar: MatSnackBar,
    private route: ActivatedRoute,
    private router: Router
    ) { }

  ngOnInit() {
    this.form = new FormGroup({
      'id' : new FormControl(''),
      'dni' : new FormControl('',[Validators.required, Validators.minLength(7)]),
      'nombre' : new FormControl('', Validators.required),
    });

    this.route.params.subscribe((params:Params) =>{
      this.id = params['id'];
      this.edicion = this.id != null;
      this.initForm();
    });
  }

  initForm(){
    if(this.edicion){
      this.clienteService.leer(this.id).pipe(takeUntil(this.ngUnsubscribe)).subscribe((data:Cliente) =>{
        this.form = new FormGroup({
          'id' : new FormControl(data.id),
          'dni' : new FormControl(data.dni),
          'nombre' : new FormControl(data.nombreCompleto),
        });

        
        if(data.id != null){
          this.afStorage.ref(`clientes/${data.id}`).getDownloadURL().subscribe(data =>{
            if(data != null){
              this.urlImagen = data;
            }
          }, err =>{
            if(err.code === "storage/object-not-found"){
              this.snackBar.open('El cliente no cuenta con una imagen', 'AVISO',{
                duration: 2000
              });
            }
          });
        }
      });
    }
  }

  get f (){ return this.form.controls; }

  operar(){
    if (!this.form.invalid) {
      let cliente = new Cliente();

      if(this.edicion){
        cliente.id = this.form.value['id'];
      }else{
        cliente.id = this.afStote.createId();
      }
  
      cliente.dni = this.form.value['dni'];
      cliente.nombreCompleto = this.form.value['nombre'];
  
      if(this.file != null){
        let ref = this.afStorage.ref(`clientes/${cliente.id}`);
        ref.put(this.file);
      }
  
      let mensaje;
  
      if(this.edicion){
        this.clienteService.actualizar(cliente);
        mensaje = 'SE MODIFICO';
      }else{
        this.clienteService.registrar(cliente);
        mensaje = 'SE REGISTRO';
      }
  
      this.snackBar.open(mensaje, 'AVISO',{
        duration: 2000
      });
  
      this.router.navigate(['cliente']);
    }
  }

  seleccionar(e: any){
    this.file = e.target.files[0];
    this.labelFIle = e.target.files[0].name;
  }

  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
   }

}
