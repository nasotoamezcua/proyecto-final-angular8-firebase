import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ClienteService } from 'src/app/_service/cliente.service';
import { Cliente } from 'src/app/_model/cliente';
import { MatTableDataSource, MatPaginator, MatSort, MatSnackBar } from '@angular/material';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-cliente',
  templateUrl: './cliente.component.html',
  styleUrls: ['./cliente.component.css']
})
export class ClienteComponent implements OnInit, OnDestroy {

  private ngUnsubscribe: Subject<void> = new Subject();

  dataSource: MatTableDataSource<Cliente>;
  displayedColumns: string[] = ['dni', 'nombreCompleto', 'acciones'];

  @ViewChild(MatPaginator, {static :false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: false}) sort: MatSort;

  constructor(
    private clienteService: ClienteService,
    public activateRoute: ActivatedRoute,
    private snackBar: MatSnackBar) { }

  ngOnInit() {

    this.clienteService.listar().pipe(takeUntil(this.ngUnsubscribe)).subscribe(data =>{
      this.dataSource = new MatTableDataSource(data);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      console.log(data);
    });
  }

  eliminar(cliente: Cliente){
    this.clienteService.eliminar(cliente).then(() =>{
      this.snackBar.open('SE ELIMINO', 'AVISO',{
        duration: 2000,
      });
    });

  }

  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
   }
}
